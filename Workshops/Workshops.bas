Attribute VB_Name = "Module1"
Option Explicit

Sub ExportToJPEG()
    Dim nTotalPages     As Integer
    Dim nPage           As Integer
    Dim sPageNo         As String
    Dim sPageFileName   As String
    
    'Ask whether to proceed or not
    'If they pressed OK then proceed, otherwise do nothing.
    If MsgBox("Save all pages as individual pictures?", vbOKCancel) = vbOK Then
    
        'Make sure two-page spread is set to FALSE, otherwise it might
        'export the two pages as one picture, instead of individually.
        'NOTE: This may not be necessary, but just in case!
        ActiveDocument.ViewTwoPageSpread = False
        
        'How many total pages in document?
        nTotalPages = ActiveDocument.Pages.Count
        
        'Loop thru all pages one at a time
        For nPage = 1 To nTotalPages
            
            sPageFileName = ActiveDocument.FullName
            'When you convert integer to string it adds a space in front, so need to remove the leading space
            
            'Add the appropriate fileformat extension. You can use .png and .jpg for sure. There may be others, but did not test any.
            sPageFileName = Replace(sPageFileName, ".pub", "_Pg" & Format(nPage, "00") & ".jpg")
            
            'Save the page
            ActiveDocument.Pages(nPage).SaveAsPicture (sPageFileName)
        
        Next nPage
        
        'Tell them you are done and how many pages were saved
        MsgBox "DONE! Saved" + Str(nTotalPages) + " pages."
    
    End If

End Sub
