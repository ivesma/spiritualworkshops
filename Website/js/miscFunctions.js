/*  -----------------------------------------------------------
    General functions used through out the site
    -----------------------------------------------------------*/

function mailTo(emailName, subject) {
    // the following expression must be all on one line...
    var sBody = "I would like to make the following enquires...";
    window.location = "mailto:"+emailName+"@geraldinevanheemstra.com?subject="+subject+"&body="+sBody;
}

function goToURL(targetUrl) {
    window.location = targetUrl; 
}