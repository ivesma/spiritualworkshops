/*  -----------------------------------------------------------
    Site Template functions
    -----------------------------------------------------------*/
// Test to see if menu item has children
function menuHasChildren(oMenu) {
    return (oMenu.next("div.menu_body").length > 0);
}

// Prepare each menu header
function setMenuImage(oSelected, imageURL) {
    if (menuHasChildren(oSelected)) {
        oSelected.css({backgroundImage:"url("+imageURL+")"});
    }
}
// Read a page's GET URL variables and return them as an associative array.
// Usage: 
// Get object of URL parameters
//var allVars = $.getUrlVars();
//
// Getting URL var by its nam
//var byName = $.getUrlVar('name');

function getUrlParam( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

function fullScreen(URL) {
	window.open(URL, '', 'fullscreen=yes, scrollbars=auto');
}

function replaceAll( strSearchIn, strSearchFor, strReplaceWith ) {
    var strReplaceAll = strSearchIn;
    var intIndexOfMatch = strReplaceAll.indexOf( strSearchFor );
     
    // Loop over the string value replacing out each matching
    // substring.
    while (intIndexOfMatch != -1){
        // Relace out the current instance.
        strReplaceAll = strReplaceAll.replace( strSearchFor, strReplaceWith )
         
        // Get the index of any next matching substring.
        intIndexOfMatch = strReplaceAll.indexOf( strSearchFor );
    }
     
    return strReplaceAll;    
}

