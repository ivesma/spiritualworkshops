<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Spiritual Workshops / Talking Peace</title>
    </head>

    <body>
	<?php
        // get variables
        
        // link href='download.php?name=myfile.mp3&path=music/myfile.mp3'
        $saveName = stripslashes($_GET['name']);
        $savePath = stripslashes($_GET['path']);

        // send headers to initiate a binary download

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=$saveName');
        header('Content-Transfer-Encoding: binary');
        header('Content-length: ' . filesize($savePath));

        // read file

        readfile($savePath);
    ?>
    </body>
</html>