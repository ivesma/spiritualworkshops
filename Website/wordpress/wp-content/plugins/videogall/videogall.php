<?php
/*
Plugin Name: VideoGall
Plugin URI: http://nischalmaniar.com/videogall
Version: 2.4.1
Author: Nischal Maniar
Author URI: http://www.nischalmaniar.com
*/

/*  Copyright 2009

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/* Fix to avoid fatal memory error */
ini_set("memory_limit","50M");
//Defining Variables
global $wpdb, $video_table_name, $category_table_name, $videogall_db_version, $videocat_db_version;
define('VIDEOGALL_DIR_URL',plugin_dir_url(__FILE__));
define('VIDEOGALL_DIR_PATH',plugin_dir_path(__FILE__));
$videogall_shortname = 'videogall';
$video_table_name = $wpdb->prefix.'videogall';
$category_table_name = $wpdb->prefix.'vidcategory';
$videogall_db_version = '1.9';
$videocat_db_version = '1.2';

/** Function called when plugin is activated **/
/*****************************************************************/
/*** DO NOT MODIFY THIS FUNCTION ***/
/***********************************/
function videogall_install() {
    global $wpdb, $video_table_name, $category_table_name, $videogall_db_version, $videocat_db_version;
    if($wpdb->get_var("SHOW TABLES LIKE '$video_table_name'") != $video_table_name) {
        $sql = "CREATE TABLE ".$video_table_name." (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            url text NOT NULL,
            caption text,
            category text NOT NULL,
            thumbnail text,
            name text NOT NULL,
	    description text,
            UNIQUE KEY id (id)
        );";
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        add_option('videogall_db_version', $videogall_db_version);
    } else {
        $installedversion = get_option('videogall_db_version');
	if($installedversion != $videogall_db_version) {
	    $sql = "CREATE TABLE ".$video_table_name." (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		url text NOT NULL,
		caption text,
		category text NOT NULL,
		thumbnail text,
		name text NOT NULL,
		description text,
		UNIQUE KEY id (id)
	      );";
	    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
	    dbDelta($sql);
            update_option('videogall_db_version',$videogall_db_version);
	}
    }

    if($wpdb->get_var("SHOW TABLES LIKE '$category_table_name'") != $category_table_name) {
	$catsql = "CREATE TABLE ".$category_table_name." (
	    catid mediumint(9) NOT NULL AUTO_INCREMENT,
	    catname text NOT NULL,
	    UNIQUE KEY catid (catid)
	);";
	require_once(ABSPATH.'wp-admin/includes/upgrade.php');
	dbDelta($catsql);
        add_option('videocat_db_version', $videocat_db_version);
    } else {
	$installedversion = get_option('videocat_db_version');
	if($installedversion != $videocat_db_version) {
	    $catsql = "CREATE TABLE ".$category_table_name." (
		catid mediumint(9) NOT NULL AUTO_INCREMENT,
		catname text NOT NULL,
		UNIQUE KEY catid (catid)
	    );";
	    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
	    dbDelta($catsql);
            update_option('videocat_db_version',$videocat_db_version);
	}
    }
}
register_activation_hook(__FILE__,'videogall_install');

/** Calling the Settings page and DB functions **/
require(VIDEOGALL_DIR_PATH.'inc/videogall-functions.php');
require(VIDEOGALL_DIR_PATH.'inc/plugin-settings.php');
$videogall_options = videogall_get_options();

$videosize = explode("|",$videogall_options['video_size']);
$videowidth = $videosize[0];
$videoheight = $videosize[1];
$videorel = ';width='.$videowidth.';height='.$videoheight;

function videogall_display_gallery($category = '', $class = 'videogall-thumb', $iswidget = false, $limit = 5) {
    global $videogall_options, $thumbstyle, $videorel;
    $videos = videogall_list_videos($category,$videogall_options['arrange_field'],$videogall_options['arrange_type']);
    $output = '';
    $total_videos = videogall_count_videos();
    $count = 1;
    if(!$iswidget) {
        $thumbstyle = videogall_thumb_size();
    } else $thumbstyle = '';
    foreach($videos as $video) {
        if($video->thumbnail == '') {
            $thumb = VIDEOGALL_DIR_URL.'images/default.jpg';
        } else $thumb = $video->thumbnail;

        $output .= '<div id="videogall-thumb-'.$video->id.'" class="'.$class.'">';
        $output .= '<a rel="shadowbox['.$video->category.']'.$videorel.'" href="'.$video->url.'">
                        <img class="'.$class.'-img" src="'.$thumb.'" '.$thumbstyle.'/>
                    </a>';
        if(trim($video->caption) != '')
            $output .= '<p class="videogall-caption">'.$video->caption.'</p>';
        if(trim($video->description) != '' and !$iswidget)
            $output .= '<p class="videogall-description">'.$video->description.'</p>';
        $output .= '</div>'."\n\n";
        if((!$iswidget and $count % $videogall_options['videos_per_page']) == 0 and $count < $total_videos and $videogall_options['videos_per_page'] < $total_videos and $videogall_options['enable_pagination']) $output.= '<!--nextpage-->';
        if($iswidget and $count == $limit) break;
        $count++;
    }
    return $output;
}

function videogall_horizontal_inline_styles() {
    global $videogall_options, $thumbwidth, $thumbheight;
    $thumbsize = explode("|",$videogall_options['thumbnail_size']);
    $thumbwidth = $thumbsize[0];
    $thumbheight = $thumbsize[1];
    $style = '<style>'."\n";
    $style .= "\t".'.videogall-thumb { width: '.$thumbwidth.'px; min-height: '.$thumbheight.'px; _height: '.$thumbheight.'px; }'."\n";
    if($videogall_options['enable_border']) {
        $style .= "\t".'.videogall-thumb img, .videogall-widget-thumb img { border:6px #'.$videogall_options['border_color'].' solid; }'."\n";
    }
    $style .= '</style>'."\n";
    echo $style;
}

function videogall_thumb_size() {
    global $videogall_options;
    $thumbsize = explode("|",$videogall_options['thumbnail_size']);
    $thumbwidth = $thumbsize[0];
    $thumbheight = $thumbsize[1];
    $thumbstyle = 'style="width:'.$thumbwidth.'px; height:'.$thumbheight.'px;"';
    return $thumbstyle;
}

function videogall_enqueue_scripts() {
    global $videogall_options;
    echo '<link rel="stylesheet" type="text/css" href="'.VIDEOGALL_DIR_URL.'videogall-style.css">'."\n";
    echo '<link rel="stylesheet" type="text/css" href="'.VIDEOGALL_DIR_URL.'inc/shadowbox/shadowbox.css">'."\n";
    echo '<script type="text/javascript" src="'.VIDEOGALL_DIR_URL.'inc/shadowbox/shadowbox.js"></script>'."\n";
    echo '<script type="text/javascript">'."\n";
    echo "\t".'jQuery(document).ready(function() {'."\n";
    echo "\t".'Shadowbox.init();'."\n";
    echo "\t".'});'."\n";
    echo '</script>'."\n";
}

function videogall_output($content) {
    global $videogall_options;
    $gallery = preg_match_all('/\[myvideogall:(.*)\]/',$content,$matches);
    for($i=0;$i<count($matches[0]);$i++) {
      $count++;
      $tag = explode(":",$matches[0][$i]);
      $category = str_replace(']','',$tag[1]);
      if($category == "all") $filtercategory = ""; else $filtercategory = $category;
      $content = str_ireplace("[myvideogall:$category]",videogall_display_gallery($filtercategory),$content);
    }
    if($videogall_options['enable_pagination'])
        $content = videogall_paginate($content);
    return $content;
}

function videogall_add_image_shadowbox ($content) {
    $pattern = "/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i";
    $replacement = '<a$1href=$2$3.$4$5 rel="shadowbox"$6>$7</a>';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}

function videogall_add_shortcode() {
    if (current_user_can('edit_posts') && current_user_can('edit_pages')) {
        add_filter('mce_external_plugins', 'videogall_add_shortcode_plugin');
        add_filter('mce_buttons', 'videogall_register_shortcode');
    }
}

function videogall_register_shortcode($buttons) {
    array_push($buttons,"videogall");
    return $buttons;
}

function videogall_add_shortcode_plugin($plugin_array) {
    $plugin_array['videogall'] = VIDEOGALL_DIR_URL.'js/videogall-shortcodes.js';
    return $plugin_array;
}

function videogall_activate_widget() {
    require(VIDEOGALL_DIR_PATH.'videogall-widget.php');
}

wp_enqueue_script('jquery');
add_action('wp_head','videogall_enqueue_scripts');
add_action('wp_head','videogall_horizontal_inline_styles');
add_action('init','videogall_add_shortcode'); //For Shortcodes
add_filter('the_content','videogall_output');
add_action('widgets_init','videogall_activate_widget');
if($videogall_options['enable_shadowbox_images']) {
    add_filter('the_content','videogall_add_image_shadowbox');
}
/** Making the plugin translation ready **/
$plugin_lang_dir = basename(dirname(__FILE__))."/languages";
load_plugin_textdomain('videogall', null, $plugin_lang_dir);
?>