/* PLUGIN SETTINGS JAVASCRIPT */

jQuery(document).ready(function() {

    //Loading current tab using cookie
    if(getCookie("currtab") != "" && getCookie("currtab") != null) {
        jQuery('.settings-tab').removeClass('current-tab');
        var currtab = getCookie("currtab");
        jQuery('#' + currtab).addClass('current-tab');
        var currsection = currtab.replace('settings-tab','settings');
        jQuery('.settings-section').hide();
        jQuery('#' + currsection).show();
    }

    //Animating tabs
    jQuery(".settings-tab").click(function() {
        var currentId = jQuery(".current-tab").attr("id");
        var clickedId = jQuery(this).attr("id");
        var clickedSection = clickedId.replace("settings-tab","settings");
        if(clickedId != currentId) {
            jQuery("#" + currentId).removeClass("current-tab");
            jQuery("#" + clickedId).addClass("current-tab");
            jQuery(".settings-section").hide();
            jQuery("#" + clickedSection).show();
        }
    });

    //Upload image script
    var formfieldID;
    jQuery('.upload_img').click(function() {
	var btnId = jQuery(this).attr("id");
	formfieldID = btnId.replace("_button","");
	tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
	return false;
    });
    window.send_to_editor = function(html) {
	imgurl = jQuery('img',html).attr('src');
	jQuery('#' + formfieldID).val(imgurl);
	jQuery('#' + formfieldID + '_error').html("");
        tb_remove();
    }

    //Validations
    jQuery('.img_field').blur(function() {
	var errorId = jQuery(this).attr("id") + '_error';
	var value = jQuery(this).val();
	var pattern = new RegExp("http:\\/\\/\\S+\\.[jJ][pP][eE]?[gG]");
	if(jQuery.trim(value) != "" && !pattern.test(value)) {
	    jQuery('#' + errorId).html("Invalid image url");
            jQuery('#' + errorId).fadeIn(700);
	} else {
            jQuery('#' + errorId).fadeOut(700);
	    jQuery('#' + errorId).html("");
	}
    });

    //Edit Category script
    jQuery('#edit_category_button').click(function() {
	jQuery('#edit_category_text_row').show();
	jQuery('#edit_category_name').val("");
    });
    jQuery('#cancel_edit_category_button').click(function() {
	jQuery('#edit_category_text_row').hide();
	jQuery('#edit_category_name').val("");
    });

    //Edit Video script
    var editVideoIdList = jQuery('#edit_video_id').val();
    jQuery('.edit_video_button').click(function() {
	var videoid = jQuery(this).attr("id").replace('edit_video_button_','');
	jQuery('#edit_video_name_' + videoid).removeAttr("readonly");
	jQuery('#edit_video_url_' + videoid).removeAttr("readonly");
	jQuery('#edit_video_category_' + videoid).attr("disabled",false);
	jQuery('#edit_video_thumbnail_' + videoid).removeAttr("readonly");
	jQuery('#edit_video_thumbnail_' + videoid + '_button').attr("disabled",false);
	jQuery('#edit_video_caption_' + videoid).removeAttr("readonly");
	jQuery('#edit_video_desc_' + videoid).removeAttr("readonly");

	jQuery('#edit_video_button_' + videoid).hide();
	jQuery('.editabletext').removeClass('readonlytext');

	editVideoIdList = editVideoIdList + videoid + ",";
	jQuery('#edit_video_id').val(editVideoIdList);
    });

    //Delete video script
    jQuery('.delete_video_button').click(function() {
	var videoid = jQuery(this).attr("id").replace('delete_video_button_','');
	jQuery('#edit_video_id').val(videoid);
    });

    //Setting cookie to remember last tab
    jQuery('#submit').click(function() {
        var currentTabId = jQuery('.current-tab').attr("id");
        setCookie("currtab",currentTabId,3);
    });

    jQuery('#delete_category_button').click(function() {
        var currentTabId = jQuery('.current-tab').attr("id");
        setCookie("currtab",currentTabId,3);
    });

    jQuery('#add_new_video_button').click(function() {
        var currentTabId = jQuery('.current-tab').attr("id");
        setCookie("currtab",currentTabId,3);
    });

    jQuery('#save_category_update').click(function() {
        var currentTabId = jQuery('.current-tab').attr("id");
        setCookie("currtab",currentTabId,3);
    });

});

function confirmAction() {
    var confirmation = confirm("Do you want to delete your settings and restore to default settings ? (Note: This won't delete your videos)");
    return confirmation;
}

function setCookie(name,value,secs) {
    if (secs) {
	var date = new Date();
	date.setTime(date.getTime()+(secs*1000));
	var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}