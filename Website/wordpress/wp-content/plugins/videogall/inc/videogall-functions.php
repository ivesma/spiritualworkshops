<?php
/* DATABASE FUNCTIONS */
/* DO NOT MODIFY THIS FILE */
/**********************************************/

/**
 * Retrieve count of videos
 */
function videogall_count_videos($name = '') {
    global $wpdb, $video_table_name;
    if(trim($name) != '') $filter = " WHERE UPPER(name) = '".strtoupper($name)."';"; else $filter = ';';
    $sql = "SELECT COUNT(*) AS CNT FROM ".$video_table_name.$filter;
    $count = $wpdb->get_results($sql);
    return $count[0]->CNT;
}

/**
 * Retrieve Videos
 */
function videogall_list_videos($category = '', $sortfield = 'id', $sorttype = 'desc') {
    global $wpdb, $video_table_name;
    $sort = " ORDER BY ".strtolower($sortfield)." ".strtolower($sorttype).";";
    if(trim($category) != '') $filter = " WHERE UPPER(category) = '".strtoupper($category)."'".$sort; else $filter = $sort;
    $sql = "SELECT * FROM ".$video_table_name.$filter;
    $results = $wpdb->get_results($sql);
    return $results;
}

/**
 * Edit Videos
 */
function videogall_edit_videos($id, $action, $name='', $url='', $category='', $thumbnail='', $caption='', $description='') {
    global $wpdb, $video_table_name;
    if($action == 'edit') {
        if(trim($thumbnail) == "") {
            $thumbnail = videogall_process_url($url);
            $thumbnail = $thumbnail['thumb'];
        }
        $sql = "SELECT id FROM ".$video_table_name." WHERE UPPER(name) = '".strtoupper($name)."';";
        $results = $wpdb->get_results($sql);
        if($results[0]->id != $id) {
            if(videogall_count_videos($name) > 0)
                return array('success'=>false,'message'=>__('Video with name: &quot;'.$name.'&quot; already exists. Try another name','videogall'));
        }
        
        $processed_video = videogall_process_url($url);
        if(trim($thumbnail) == "") {
            $thumbnail = $processed_video['thumb'];
        }
        
        $sql = "UPDATE ".$video_table_name." SET name = '".$name."', url = '".$processed_video['url']."', category = '".ucwords($category)."', thumbnail = '".$thumbnail."', caption = '".ucwords($caption)."', description = '".ucwords($description)."' WHERE id = ".$id.";";
        $results = $wpdb->query($sql);
        if($results != 1) {
            return array('success'=>true,'message'=>__('Videos updated successfully','videogall'));
        } else {
            return array('success'=>true,'message'=>__('Videos updated successfully','videogall'));
        }
    } elseif($action == 'delete') {
        $sql = "DELETE FROM ".$video_table_name." WHERE id = ".$id.";";
        $results = $wpdb->query($sql);
        if($results != 1) {
            return array('success'=>false,'message'=>__('Video cannot be deleted','videogall'));
        } else {
            return array('success'=>true,'message'=>__('Video has been deleted','videogall'));
        }
    }
}

/**
 * Add Videos
 */
function videogall_add_video($name,$url,$category,$thumbnail='',$caption='',$description='') {
    global $wpdb, $video_table_name;
    if(videogall_count_videos($name) > 0)
        return array('success'=>false,'message'=>__('Video with name: &quot;'.$name.'&quot; already exists. Try another name','videogall'));
    else {
        $processed_video = videogall_process_url($url);
        if(trim($thumbnail) == "") {
            $thumbnail = $processed_video['thumb'];
        }
        $sql = "INSERT INTO ".$video_table_name." (name,url,category,thumbnail,caption,description) VALUES ('".$name."','".$processed_video['url']."','".$category."','".$thumbnail."','".$caption."','".$description."');";
        $results = $wpdb->query($sql);
        if($results != 1) {
            return array('success'=>false,'message'=>__('Video &quot;'.$name.'&quot; cannot be added','videogall'));
        } else {
            return array('success'=>true,'message'=>__('Video &quot;'.$name.'&quot; added successfully','videogall'));
        }
    }
}

/**
 * Video Category Functions
 */
function videogall_add_category($category) {
    global $wpdb, $category_table_name;
    $existing_list = videogall_list_categories($category);
    if(!empty($existing_list))
        return array('success'=>false,'message'=>__('Category "'.$category.'" already exists','videogall'));
    else {
        $sql = "INSERT INTO ".$category_table_name." (catname) VALUES ('".$category."')";
        $results = $wpdb->query($sql);
        if($results != 1) {
            return array('success'=>false,'message'=>__('Category &quot;'.$category.'&quot; cannot be added','videogall'));
        } else {
            return array('success'=>true,'message'=>__('Category &quot;'.$category.'&quot; added successfully','videogall'));
        }
    }
}

function videogall_edit_category($id, $action, $category = '') {
    global $wpdb, $category_table_name, $video_table_name;
    if($action == 'delete') {
        $sql = "SELECT catname FROM ".$category_table_name." WHERE catid = ".$id.";";
        $results = $wpdb->get_results($sql);
        $deleteCategoryName = $results[0]->catname;

        $sql = "UPDATE ".$video_table_name." SET category = 'Uncategorized' WHERE UPPER(category) = '".strtoupper($deleteCategoryName)."';";
        $results = $wpdb->query($sql);

        $sql = "DELETE FROM ".$category_table_name." WHERE catid = ".$id.";";
        $results = $wpdb->query($sql);

        if($results != 1) {
            return array('success'=>false,'message'=>__('Category cannot be deleted','videogall'));
        } else {
            return array('success'=>true,'message'=>__('Category has been deleted','videogall'));
        }
    } elseif($action == 'update') {
        $existing_list = videogall_list_categories($category);
        if(!empty($existing_list))
            return array('success'=>false,'message'=>__('Category "'.$category.'" already exists','videogall'));
        else {
            $sql = "SELECT catname FROM ".$category_table_name." WHERE catid = ".$id.";";
            $results = $wpdb->get_results($sql);
            $oldCategoryName = $results[0]->catname;

            $sql = "UPDATE ".$video_table_name." SET category = '".$category."' WHERE UPPER(category) = '".strtoupper($oldCategoryName)."';";
            $results = $wpdb->query($sql);

            $sql = "UPDATE ".$category_table_name." SET catname = '".$category."' WHERE catid = ".$id.";";
            $results = $wpdb->query($sql);

            if($results != 1) {
                return array('success'=>false,'message'=>__('Category &quot;'.$category.'&quot; cannot be renamed','videogall'));
            } else {
                return array('success'=>true,'message'=>__('Category &quot;'.$oldCategoryName.'&quot; renamed to &quot;'.$category.'&quot;','videogall'));
            }
        }
    }
}

function videogall_list_categories($category = '') {
    global $wpdb, $category_table_name;
    if(trim($category) != '') $filter = " WHERE UPPER(catname) = '".strtoupper($category)."';"; else $filter = ';';
    $sql = "SELECT * FROM ".$category_table_name.$filter;
    $results = $wpdb->get_results($sql);
    return $results;
}

/*
 * Video Meta Functions
 */
function videogall_process_url($url) {
    $thumb = '';
    $newurl = '';
    //Getting youtube's thumbnail
    if(preg_match('/youtube/',$url) > 0 or preg_match('/youtu.be/',$url) > 0) {
        if(preg_match('/embed/',$url) > 0) {
            $pos = strpos($url,'?');
            if($pos > 0)
                $url = substr($url,0,$pos);
            $pos = strpos($url,'&');
            if($pos > 0)
                $url = substr($url,0,$pos);
            $url = str_ireplace('http://www.youtube.com/embed/','',$url);
            $url = str_ireplace('http://youtube.com/embed/','',$url);
        } else if(preg_match('/watch/',$url) > 0) {
            $pos = strpos($url,'&');
            if($pos > 0)
                $url = substr($url,0,$pos);
            $url = str_ireplace('http://www.youtube.com/watch?v=','',$url);
            $url = str_ireplace('http://youtube.com/watch?v=','',$url);
        } else {
            $url = str_ireplace('http://www.youtube/','',$url);
            $url = str_ireplace('http://youtube/','',$url);
            $url = str_ireplace('http://youtu.be/','',$url);
        }
        $url = str_ireplace('/','',$url);
        $newurl = 'http://www.youtube.com/embed/'.$url;
        $thumb = 'http://img.youtube.com/vi/'.$url.'/default.jpg';
    }
    elseif(preg_match('/metacafe/',$url) > 0) {
        $url = str_ireplace('http://www.metacafe.com/watch/','',$url);
        $url = str_ireplace('http://metacafe.com/watch/','',$url);
        $url = rtrim($url,'\/');
        if(preg_match('/metacafe\.com\/fplayer/',$url) > 0)
            $newurl = $url;
        else
            $newurl = 'http://www.metacafe.com/fplayer/'.$url.'.swf';
        $pos = strpos($url,'/');
        if($pos > 0)
            $url = substr($url,0,$pos);
        $url = str_ireplace('/','',$url);
        $thumb = 'http://www.metacafe.com/thumb/'.$url.'.jpg';
    }
    elseif(preg_match('/dailymotion/',$url) > 0) {
        $thumb = str_ireplace('video','thumbnail/video',$url);
        $user = str_ireplace('http://www.dailymotion.com/video/','',$url);
        $pos = strpos($user,'_');
        if($pos > 0)
            $user = substr($user,0,$pos);
        if(preg_match('/dailymotion.com\/embed\/video/',$url) > 0)
            $newurl = $url;
        else
            $newurl = 'http://www.dailymotion.com/embed/video/'.$user;
    }
    elseif(preg_match('/vimeo/',$url) > 0) {
        $url = str_ireplace('http://www.vimeo.com/','',$url);
        $url = str_ireplace('http://vimeo.com/','',$url);
        if(preg_match('/player\.vimeo\.com/',$url) > 0)
            $newurl = $url;
        else
            $newurl = 'http://player.vimeo.com/video/'.$url;
        $thumb = getVimeoThumb($url);
    }
    elseif(preg_match('/google/',$url) > 0) {
        $url = str_replace('http://video.google.com/videoplay?docid=','',$url);
        $url = str_replace('http://www.video.google.com/videoplay?docid=','',$url);
        $url = rtrim($url,'\/');
        $newurl = 'http://video.google.com/googleplayer.swf?docid='.$url;
        $thumb = VIDEOGALL_DIR_URL.'images/default.jpg';
    }
    elseif(videogall_is_wordpress($url)) {
        $newurl = videogall_wordpress_url($url);
        $thumb = VIDEOGALL_DIR_URL.'images/default.jpg';
    }
    else {
        $newurl = $url;
        $thumb = VIDEOGALL_DIR_URL.'images/default.jpg';
    }
    return array('url'=>$newurl,'thumb'=>$thumb);
}

/* SUPPORTING FUNCTIONS */

function getVimeoThumb($url) {
    $apiurl = 'http://vimeo.com/api/v2/video/'.$url.'.php';
    $contents = @file_get_contents($apiurl);
    $array = @unserialize(trim($contents));
    return $array[0][thumbnail_medium];
}

/**
 * Checks if url is from wordpress blog
 */
function videogall_wordpress_url($url) {
    global $site_content;
    if(preg_match('/\<embed.*src\=\"(.+)\".*\>.*\<\/embed\>/',$site_content,$matches) > 0) {
        $pos = strpos($matches[1],'"');
        $url = substr($matches[1],0,$pos);
    }
    if(preg_match('/\<iframe.*src\=\"(.+)\".*\>.*\<\/iframe\>/',$site_content,$matches) > 0) {
        $pos = strpos($matches[1],'"');
        $url = substr($matches[1],0,$pos);
    }
    return $url;
}

function videogall_is_wordpress($url) {
    global $site_content;
    videogall_get_site_data($url);
    if(preg_match('/\<meta(.*)content\=\"WordPress/',$site_content) > 0)
        return true;
    else return false;
}

function videogall_get_site_data($url) {
    global $site_content;
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
    $site_content = curl_exec($ch);
    curl_close($ch);
}

/**
 * Paginate Videos
 */
function videogall_paginate($content) {
    $paged_content = explode('<!--nextpage-->',$content);
    $total_pages = count($paged_content);
    $pagenumber = 1;
    $videonav = '';
    $url = get_permalink();
    $currentpage = videogall_current_url();
    if($total_pages > 1) {
        if(preg_match('/\?/',$url) > 0) {
            $pagelink = '&page=';
        } else {
            $pagelink = '?page=';
        }
        $pagenumber = $_GET['page'];
        if(!is_numeric($pagenumber) or $pagenumber == '') $pagenumber = 1;
        $content = $paged_content[($pagenumber-1)];
        $videonav .= '<div class="video-navigation clearfix">';
        for($i = 1; $i <= $total_pages; $i++) {
            if($i < 2) $linkurl = $url; else $linkurl = $url.$pagelink.$i;
            if($linkurl == $currentpage) $current = 'class="current-video-page"'; else $current = '';
            $videonav .= '<a href="'.$linkurl.'" '.$current.'>'.$i.'</a>';
        }
        $videonav .= '</div><br/>';
        $content = $content.$videonav;
    }
    return $content;
}

function videogall_current_url() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

/*
 * Resizes images on fly
 */
function videogall_image_resize($width, $height, $target) {
    if ($width > $height) {
        $percentage = ($target / $width);
    } else {
        $percentage = ($target / $height);
    }
    $width = round($width * $percentage);
    $height = round($height * $percentage);
    return "width=\"$width\" height=\"$height\"";
}
?>