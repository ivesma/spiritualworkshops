<?php
$videogall_video_size_list = array(
    '480|320' => array('value' => '480|320', 'label' => __('Small','videogall')),
    '640|520' => array('value' => '640|520', 'label' => __('Medium','videogall')),
    '1024|720' => array('value' => '1024|720', 'label' => __('Large','videogall')),
);

$videogall_thumb_size_list = array(
    '150|120' => array('value' => '150|120', 'label' => __('Small','videogall')),
    '230|200' => array('value' => '230|200', 'label' => __('Medium','videogall')),
    '350|320' => array('value' => '350|320', 'label' => __('Large','videogall')),
);

$videogall_arrange_field_list = array(
    'name' => array('value' => 'name', 'label' => __('Name','videogall')),
    'id' => array('value' => 'id', 'label' => __('Date Uploaded','videogall')),
    'category' => array('value' => 'category', 'label' => __('Category','videogall')),
);

$videogall_arrange_type_list = array(
    'asc' => array('value' => 'asc', 'label' => __('Ascending','videogall')),
    'desc' => array('value' => 'desc', 'label' => __('Descending','videogall')),
);

//Manage your tab list here
$videogall_tab_list = array(
    'video' => array('value' => 'video', 'label' => __('Videos','videogall')),
    'addvideo' => array('value' => 'addvideo', 'label' => __('Add New Video','videogall')),
    'category' => array('value' => 'category', 'label' => __('Categories','videogall')),
    'additional' => array('value' => 'additional', 'label' => __('Additional Settings','videogall')),
    'instructions' => array('value' => 'instructions', 'label' => __('Instructions','videogall'))
);

add_action('admin_init', 'videogall_init_options');
add_action('admin_menu', 'videogall_activate_options_page');

function videogall_video_display_style_list() {
    global $videogall_video_display_style_list;
    return apply_filters('videogall_video_display_style_list', $videogall_video_display_style_list);
}

function videogall_video_size_list() {
    global $videogall_video_size_list;
    return apply_filters('videogall_video_size_list', $videogall_video_size_list);
}

function videogall_thumbnail_size_list() {
    global $videogall_thumb_size_list;
    return apply_filters('videogall_thumbnail_size_list', $videogall_thumb_size_list);
}

function videogall_arrange_field_list() {
    global $videogall_arrange_field_list;
    return apply_filters('videogall_arrange_field_list', $videogall_arrange_field_list);
}

function videogall_arrange_type_list() {
    global $videogall_arrange_type_list;
    return apply_filters('videogall_arrange_type_list', $videogall_arrange_type_list);
}

function videogall_tab_list() {
    global $videogall_tab_list;
    return apply_filters('videogall_tab_list', $videogall_tab_list);
}

function videogall_enqueue_admin_scripts($hook_suffix) {
    global $videogall_shortname;
    wp_enqueue_script('jquery'); //Adding the JQuery script
    wp_enqueue_script('media-upload'); //Adding the media upload script
    wp_enqueue_script('thickbox'); //Adding the thickbox script for media upload
    wp_enqueue_style('thickbox'); //Adding the thickbox stylesheet for media upload
    wp_enqueue_script($videogall_shortname.'_color_picker', VIDEOGALL_DIR_URL.'/inc/jquery-colorpicker/jscolor.js', false, false);
    wp_enqueue_script($videogall_shortname.'_admin_js', VIDEOGALL_DIR_URL.'/inc/plugin-settings.js', array('jquery'), false);
    wp_enqueue_style($videogall_shortname.'_admin_css', VIDEOGALL_DIR_URL.'/inc/plugin-settings.css', false, false, 'all');
}

function videogall_default_options() {
    $default_options = array(
    'video_size' => '640|520',
	'thumbnail_size' => '150|120',
	'enable_shadowbox_images' => false,
	'enable_border' => false,
	'border_color' => 'FFFFFF',
	'arrange_field' => 'id',
	'arrange_type' => 'desc',
	'enable_pagination' => false,
	'videos_per_page' => 10,
    );
    return apply_filters('videogall_default_options', $default_options);
}

function videogall_get_options() {
    global $videogall_shortname;
    return get_option($videogall_shortname.'_options', videogall_default_options());
}

function videogall_init_options() {
    global $videogall_shortname;
    if(false === videogall_get_options()) add_option($videogall_shortname.'_options', videogall_default_options());
    videogall_save_db_settings($_POST);
    register_setting($videogall_shortname.'_options', $videogall_shortname.'_options', 'videogall_validate_options');
}

function videogall_activate_options_page() {
    global $videogall_shortname;
    $videogall_plugin_page = add_options_page(ucwords($videogall_shortname).' Settings', ucwords($videogall_shortname).' Settings', 'manage_options', 'videogall_plugin_options', 'videogall_render_options_page');
    add_action('admin_print_styles-' . $videogall_plugin_page, 'videogall_enqueue_admin_scripts');
}

function videogall_validate_options($input) {
    global $videogall_shortname;
    $output = $defaults = videogall_default_options();

    //Validating dropdown field
    if (isset($input['video_size']) && array_key_exists($input['video_size'], videogall_video_size_list()) )
	$output['video_size'] = $input['video_size'];
    if (isset($input['thumbnail_size']) && array_key_exists($input['thumbnail_size'], videogall_thumbnail_size_list()) )
	$output['thumbnail_size'] = $input['thumbnail_size'];
    if (isset($input['arrange_field']) && array_key_exists($input['arrange_field'], videogall_arrange_field_list()) )
	$output['arrange_field'] = $input['arrange_field'];
    if (isset($input['arrange_type']) && array_key_exists($input['arrange_type'], videogall_arrange_type_list()) )
	$output['arrange_type'] = $input['arrange_type'];

    //Validating color field
    if(videogall_validate_color($input['border_color']))
	$output['border_color'] = $input['border_color'];
    else
	add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_border_color_error',__('Invalid Color','videogall'),'error');

    //Validate number field
    if(videogall_validate_number($input['videos_per_page'],1,999)) {
	$output['videos_per_page'] = trim($input['videos_per_page']);
    } else
	add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_videos_per_page_error',__('Please enter an integer value between 1 and 999 for number of videos per page','videogall'),'error');

    //Validating all the checkboxes
    $chkboxinputs = array('enable_shadowbox_images','enable_border','enable_pagination');
    foreach($chkboxinputs as $chkbox) {
        if (!isset($input[$chkbox]))
            $input[$chkbox] = null;
        $output[$chkbox] = ($input[$chkbox] == true ? true : false);
    }

    return apply_filters('videogall_validate_options', $output, $input, $defaults);
}

function videogall_save_db_settings($input) {
    global $videogall_shortname;

    if(isset($input['delete_video_button'])) {
	$delete_id = trim($input['edit_video_id']);
	if(trim($delete_id) != '' and preg_match('/[A-Za-z0-9]/',$delete_id) > 0) {
	    $result = videogall_edit_videos($delete_id,'delete');
	    if(!$result['success']) add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_delete_error',$result['message'],'error');
	    else add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_delete_success',$result['message'],'updated');
	}
    }

    if(isset($input['delete_category_button'])) {
	$result = videogall_edit_category($input['edit_category_list'],'delete');
	if(!$result['success']) add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_category_delete_error',$result['message'],'error');
	else add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_category_delete_success',$result['message'],'updated');
    }

    if(isset($input['save_category_update'])) {
	$new_category = trim(wp_filter_nohtml_kses(ucwords($input['new_category_name'])));
	$rename_category = trim(wp_filter_nohtml_kses(ucwords($input['edit_category_name'])));
	$success = true;
	if($new_category == "" and $rename_category == "")
	    add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_category_edit_error',__('Enter a new category name or Rename an existing category','videogall'),'error');
	else {
	    if(trim($new_category) != "") {
		$result = videogall_add_category($new_category);
		if(!$result['success']) {
		    add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_category_edit_error',$result['message'],'error');
		    $success = false;
		}
	    }
	    if(trim($rename_category) != "") {
		$result = videogall_edit_category($input['edit_category_list'],'update',$rename_category);
		if(!$result['success']) {
		    add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_category_edit_error',$result['message'],'error');
		    $success = false;
		}
	    }
	    if($success) add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_category_edit_success',$result['message'],'updated');
	}
    }

    if(isset($input['save_video_update'])) {
	$edit_video_ids = explode(",",$input['edit_video_id']);
	$success = true;
	$result['message'] = __('Videos updated successfully','videogall');
	foreach($edit_video_ids as $video_id) {
	    if(trim($video_id) != '' and preg_match('/[A-Za-z0-9]/',$video_id) > 0) {
		$name = trim(wp_filter_nohtml_kses($input['edit_video_name_'.$video_id]));
		$url = trim(wp_filter_nohtml_kses($input['edit_video_url_'.$video_id]));
		$category = trim(wp_filter_nohtml_kses(ucwords($input['edit_video_category_'.$video_id])));
		$thumbnail = trim(wp_filter_nohtml_kses($input['edit_video_thumbnail_'.$video_id]));
		$caption = trim(wp_filter_nohtml_kses(ucwords($input['edit_video_caption_'.$video_id])));
		$description = trim(wp_filter_nohtml_kses(ucwords($input['edit_video_desc_'.$video_id])));
		if($name == "" or $url == "" or $category == "") {
		    add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_edit_error',__('Name, URL and Category are required fields','videogall'),'error');
		    $success = false;
		    continue;
		}
		$result = videogall_edit_videos($video_id,'edit',$name,$url,$category,$thumbnail,$caption,$description);
		if(!$result['success']) {
		    add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_edit_error',$result['message'],'error');
		    $success = false;
		}
	    }
	}
	if($success) add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_edit_success',$result['message'],'updated');
    }

    if(isset($input['add_new_video_button'])) {
	$name = trim(wp_filter_nohtml_kses($input['add_new_video_name']));
	$url = trim($input['add_new_video_url']);
	$category = trim(wp_filter_nohtml_kses(ucwords($input['add_new_video_category'])));
	$thumbnail = trim($input['add_new_video_thumbnail']);
	$caption = trim(wp_filter_nohtml_kses(ucwords($input['add_new_video_caption'])));
	$description = trim(wp_filter_nohtml_kses(ucwords($input['add_new_video_desc'])));
	if($name == "" or $url == "" or $category == "")
	    add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_add_video_error',__('Name, URL and Category are required fields','videogall'),'error');
	else {
	    $result = videogall_add_video($name,$url,$category,$thumbnail,$caption,$description);
	    if(!$result['success']) add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_add_error',$result['message'],'error');
	    else add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_video_add_success',$result['message'],'updated');
	}
    }
}

function videogall_render_options_page() {
    global $videogall_shortname;
    if(isset($_POST['videogall_reset_button'])) {
        delete_option($videogall_shortname.'_options');
        add_settings_error($videogall_shortname.'_options',$videogall_shortname.'_reset_options','Default settings restored','updated');
    }
    ?>
    <div class="wrap">
        <div class="icon32" id="icon-options-general"></div>
        <h2 class="plugin-title"><?php echo ucwords($videogall_shortname).__(' Settings','videogall'); ?></h2>
	<?php settings_errors(); ?>
        <hr class="line"/>
        <div class="plugin-credit">
            <p><strong><?php _e('This plugin is designed and developed by ','videogall'); ?><a href="http://www.nischalmaniar.com"><?php _e('Nischal Maniar','videogall'); ?></a></strong></p>
            <p>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" style="margin-top:10px;">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="5GY3AWKND3GTC">
                    <input style="vertical-align: middle;" type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    <label style="margin-left:5px;"><strong><?php _e('Feel free to donate as an appreciation for my time and effort to develop this plugin','videogall'); ?></strong></label>
                </form>
            </p>
        </div>

        <?php $count = 1; ?>
        <?php foreach (videogall_tab_list() as $tab) { if($count == 1) $class = " current-tab"; else $class = ""; ?>
        <div id="settings-tab-<?php echo $tab['value']; ?>" class="settings-tab<?php echo $class; ?>">
            <?php echo $tab['label']; ?>
        </div>
        <?php $count++; } ?>
        <br />
        <form class="settings-form" method="post" id="myplugin-settings-form" action="options.php">
        <?php
            settings_fields($videogall_shortname.'_options');
            $options = videogall_get_options();
            $default_options = videogall_default_options();
        ?>
        <div class="settings-section current-section" id="settings-video">
            <table>
		<?php foreach(videogall_list_videos() as $video) { ?>
		<tr>
		    <td style="border-bottom: 1px #ccc solid;">
			<?php if(trim($video->thumbnail) == '') { $thumbnail = VIDEOGALL_DIR_URL.'images/default.jpg'; } else $thumbnail = $video->thumbnail; ?>
			<img src="<?php echo $thumbnail; ?>" style="width:200px; height:auto;"/>
		    </td>
		    <td style="padding-left: 15px; border-bottom: 1px #ccc solid;">
			<p><label><?php _e('Video Name','videogall'); ?>:</label> <input size="50" class="editabletext readonlytext" readonly="readonly" name="edit_video_name_<?php echo $video->id; ?>" id="edit_video_name_<?php echo $video->id; ?>" value="<?php echo $video->name; ?>"/></p>
			<p><label><?php _e('Video URL','videogall'); ?>:</label> <input size="50" class="editabletext readonlytext" readonly="readonly" name="edit_video_url_<?php echo $video->id; ?>" id="edit_video_url_<?php echo $video->id; ?>" value="<?php echo $video->url; ?>"/></p>
			<p>
			    <label><?php _e('Video Category','videogall'); ?>:</label>
			    <select name="edit_video_category_<?php echo $video->id; ?>" id="edit_video_category_<?php echo $video->id; ?>" disabled="true">
				<option value="Uncategorized"><?php _e('Uncategorized','videogall'); ?></option>
			    <?php foreach(videogall_list_categories() as $category) { ?>
				<option value="<?php echo $category->catname; ?>" <?php selected($video->category, $category->catname); ?>><?php echo $category->catname; ?></option>
			    <?php } ?>
			    </select>
			</p>
			<p>
			    <label><?php _e('Video Thumbnail','videogall'); ?>:</label> <input size="50" class="editabletext readonlytext" class="img_field" readonly="readonly" name="edit_video_thumbnail_<?php echo $video->id; ?>" id="edit_video_thumbnail_<?php echo $video->id; ?>" value="<?php echo $thumbnail; ?>"/>
                            <input disabled="disabled" id="edit_video_thumbnail_<?php echo $video->id; ?>_button" type="button" class="upload_img button-secondary" value="<?php _e('Upload Thumbnail','videogall'); ?>" />
			</p>
			<p><label><?php _e('Video Caption','videogall'); ?>:</label> <input size="50" class="editabletext readonlytext" readonly="readonly" name="edit_video_caption_<?php echo $video->id; ?>" id="edit_video_caption_<?php echo $video->id; ?>" value="<?php echo $video->caption; ?>"/></p>
			<p><label><?php _e('Video Description','videogall'); ?>:</label> <textarea style="vertical-align:middle;" rows="2" cols="40" class="editabletext readonlytext" readonly="readonly" name="edit_video_desc_<?php echo $video->id; ?>" id="edit_video_desc_<?php echo $video->id; ?>"><?php echo $video->description; ?></textarea></p>
			<p>
			    <input type="button" class="button-secondary edit_video_button" name="edit_video_button_<?php echo $video->id; ?>" id="edit_video_button_<?php echo $video->id; ?>" value="<?php _e('Edit','videogall'); ?>"/>
			    <input type="submit" class="button-secondary delete_video_button" name="delete_video_button" id="delete_video_button_<?php echo $video->id; ?>" value="<?php _e('Delete','videogall'); ?>"/>
			</p>
		    </td>
		</tr>
		<?php } ?>
	    </table>
	    <br/>
	    <input type="submit" class="button-primary" name="save_video_update" id="save_video_update" value="<?php _e('Save Video Updates','videogall'); ?>"/>
        </div>

	<div class="settings-section" id="settings-addvideo">
	    <table>
                <tr>
                    <td><label><?php _e('Video Name','videogall'); ?> * :</label></td>
                    <td><input size="40" type="text" name="add_new_video_name" id="add_new_video_name" value=""/></td>
                </tr>
		<tr>
                    <td><label><?php _e('Video URL','videogall'); ?> * :</label></td>
                    <td><input size="40" type="text" name="add_new_video_url" id="add_new_video_url" value=""/></td>
                </tr>
		<tr>
                    <td><label><?php _e('Video Category','videogall'); ?> * :</label></td>
                    <td>
			<select name="add_new_video_category" id="add_new_video_category">
				<option value="Uncategorized"><?php _e('Uncategorized','videogall'); ?></option>
			    <?php foreach(videogall_list_categories() as $category) { ?>
				<option value="<?php echo $category->catname; ?>"><?php echo $category->catname; ?></option>
			    <?php } ?>
			</select>
		    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Video Thumbnail','videogall'); ?>:</label></td>
                    <td>
			<input size="40" type="text" name="add_new_video_thumbnail" id="add_new_video_thumbnail" value=""/>
			<input id="add_new_video_thumbnail_button" type="button" class="upload_img button-secondary" value="<?php _e('Upload Thumbnail','videogall'); ?>" />
		    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Video Caption','videogall'); ?>:</label></td>
                    <td><input size="40" type="text" name="add_new_video_caption" id="add_new_video_caption" value=""/></td>
                </tr>
		<tr>
                    <td><label><?php _e('Video Description','videogall'); ?>:</label></td>
                    <td><textarea rows="3" cols="40" name="add_new_video_desc" id="add_new_video_desc"></textarea></td>
                </tr>
            </table>
	    <br/>
	    <input type="submit" class="button-primary" name="add_new_video_button" id="add_new_video_button" value="<?php _e('Add Video','videogall'); ?>"/>
	    <p><?php _e('* are required fields','videogall'); ?></p>
        </div>

        <div class="settings-section" id="settings-category">
	    <table>
		<tr>
                    <td><label><?php _e('Add a new category','videogall'); ?> * :</label></td>
                    <td>
                        <input type="text" name="new_category_name" id="new_category_name" value=""/>
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Edit Category','videogall'); ?>:</label></td>
                    <td>
			<select name="edit_category_list">
			    <?php foreach(videogall_list_categories() as $category) { ?>
				<option value="<?php echo $category->catid; ?>"><?php echo $category->catname; ?></option>
			    <?php } ?>
			</select>
			<input type="button" class="button-secondary" name="edit_category_button" id="edit_category_button" value="<?php _e('Rename','videogall'); ?>"/>
			<input type="submit" class="button-secondary" name="delete_category_button" id="delete_category_button" value="<?php _e('Delete','videogall'); ?>"/>
                    </td>
                </tr>
		<tr id="edit_category_text_row">
                    <td><label><?php _e('New name for category','videogall'); ?> * :</label></td>
                    <td>
                        <input type="text" name="edit_category_name" id="edit_category_name" value=""/>
			<input type="button" class="button-secondary" name="cancel_edit_category_button" id="cancel_edit_category_button" value="<?php _e('Cancel','videogall'); ?>"/>
                    </td>
                </tr>
            </table>
	    <br/>
	    <input type="submit" class="button-primary" name="save_category_update" id="save_category_update" value="<?php _e('Save Categories','videogall'); ?>"/>
	    <p><?php _e('* are required fields','videogall'); ?></p>
        </div>

	<div class="settings-section" id="settings-additional">
	    <table>
                <tr>
                    <td><label><?php _e('Video Player Size','videogall'); ?>:</label></td>
                    <td>
                        <select name="<?php echo $videogall_shortname; ?>_options[video_size]">
                            <?php foreach (videogall_video_size_list() as $video_size) { ?>
                                <option value="<?php echo $video_size['value']; ?>" <?php selected( $options['video_size'], $video_size['value'] ); ?>><?php echo $video_size['label']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Thumbnail Size','videogall'); ?>:</label></td>
                    <td>
                        <select name="<?php echo $videogall_shortname; ?>_options[thumbnail_size]">
                            <?php foreach (videogall_thumbnail_size_list() as $thumbnail_size) { ?>
                                <option value="<?php echo $thumbnail_size['value']; ?>" <?php selected( $options['thumbnail_size'], $thumbnail_size['value'] ); ?>><?php echo $thumbnail_size['label']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Arrange the videos by','videogall'); ?>:</label></td>
                    <td>
                        <select name="<?php echo $videogall_shortname; ?>_options[arrange_field]">
                            <?php foreach (videogall_arrange_field_list() as $arrange_field) { ?>
                                <option value="<?php echo $arrange_field['value']; ?>" <?php selected( $options['arrange_field'], $arrange_field['value'] ); ?>><?php echo $arrange_field['label']; ?></option>
                            <?php } ?>
                        </select>

			<select name="<?php echo $videogall_shortname; ?>_options[arrange_type]">
                            <?php foreach (videogall_arrange_type_list() as $arrange_type) { ?>
                                <option value="<?php echo $arrange_type['value']; ?>" <?php selected( $options['arrange_type'], $arrange_type['value'] ); ?>><?php echo $arrange_type['label']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Enable pagination ?','videogall'); ?>:</label></td>
                    <td>
                        <input type="checkbox" name="<?php echo $videogall_shortname; ?>_options[enable_pagination]" value="true" <?php checked(true,$options['enable_pagination']); ?> />
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Number of videos per page','videogall'); ?> * :</label></td>
                    <td>
                        <input type="text" name="<?php echo $videogall_shortname; ?>_options[videos_per_page]" value="<?php echo esc_attr($options['videos_per_page']); ?>"/>
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Enable border around thumbnails ?','videogall'); ?>:</label></td>
                    <td>
                        <input type="checkbox" name="<?php echo $videogall_shortname; ?>_options[enable_border]" value="true" <?php checked(true,$options['enable_border']); ?> />
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Border Color','videogall'); ?>:</label></td>
                    <td>
                        <input type="text" name="<?php echo $videogall_shortname; ?>_options[border_color]" class="color" value="<?php echo esc_attr($options['border_color']); ?>"/>
                    </td>
                </tr>
		<tr>
                    <td><label><?php _e('Enable shadowbox for images ?','videogall'); ?>:</label></td>
                    <td>
                        <input type="checkbox" name="<?php echo $videogall_shortname; ?>_options[enable_shadowbox_images]" value="true" <?php checked(true,$options['enable_shadowbox_images']); ?> />
                    </td>
                </tr>
            </table>
	    <br/>
	    <?php submit_button(); ?>
        </div>
	<div class="settings-section" id="settings-instructions">
	    <h3><?php _e('VideoGall Installation','videogall'); ?></h3>
	    <ol>
		<li><?php _e('Unzip the VideoGall plugin in the plugins folder in wp-content directory i.e "wp-content/plugins"','videogall'); ?></li>
		<li><?php _e('Go to your wordpress site admin and plugins section and activate the VideoGall plugin','videogall'); ?></li>
		<li><?php _e('Go to settings --> VideoGall Settings to set your options and add videos','videogall'); ?></li>
		<li>
		    <?php _e('In a page or a post use the button in the editor to add the vidoeogall shortcode e.g. [myvideogall:all] will display all the videos. To display videos from a certain category, replace "all" with your desired category name','videogall'); ?>
		    <p><img src="<?php echo VIDEOGALL_DIR_URL; ?>images/shortcode_instruction1.png"/></p>
		    <p><img src="<?php echo VIDEOGALL_DIR_URL; ?>images/shortcode_instruction2.png"/></p>
		</li>
	    </ol>
	    <h3><?php _e('Adding a new video','videogall'); ?></h3>
	    <p><?php _e('Get the URL from the address bar of the video website','videogall'); ?></p>
	    <p><img src="<?php echo VIDEOGALL_DIR_URL; ?>images/video_url_instruction1.png"/></p>
	    <h3><?php _e('Supported websites','videogall'); ?></h3>
	    <p>
		<img src="<?php echo VIDEOGALL_DIR_URL; ?>images/youtube.png"/>
		<img src="<?php echo VIDEOGALL_DIR_URL; ?>images/vimeo.png"/>
		<img src="<?php echo VIDEOGALL_DIR_URL; ?>images/dailymotion.png"/>
		<img src="<?php echo VIDEOGALL_DIR_URL; ?>images/metacafe.png"/>
		<img src="<?php echo VIDEOGALL_DIR_URL; ?>images/wordpress.png"/>
	    </p>
        </div>
	<input type="hidden" name="edit_video_id" id="edit_video_id" value=""/>
        </form>
        <form class="form" method="post" id="reset-form" style="text-align: right; margin-top: 10px;" onsubmit="return confirmAction()">
            <input type="submit" class="button-secondary" name="videogall_reset_button" id="videogall_reset_button" value="<?php _e('Reset Settings','videogall'); ?>" />
        </form>
    </div>
    <?php
}

/* Supporting functions */
function videogall_validate_color($color) {
    $exp = "/([A-Za-z0-9])/";
    if(!preg_match($exp,$color))
        return false;
    else
        return true;
}

function videogall_validate_number($value,$min,$max) {
    if(is_numeric($value)) {
        $value = intval($value);
        if($value < $min or $value > $max)
            return false;
        else
            return true;
    } else return false;
}
?>