<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><!-- InstanceBegin template="/Templates/basePage.dwt" codeOutsideHTMLIsLocked="false" -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="author" content="" />
        <meta name="Keywords" content="" />
        <meta name="Description" content="" />
        <!-- InstanceBeginEditable name="PageTitle" -->
            <title>Talking Peace - Christine King's Home Page</title>
        <!-- InstanceEndEditable -->
    <meta name="Keywords" content="NVC, Nonviolent Communication, Marshall Rosenberg, Voice Dialogue, Hal and Sidra Stone, Mediation Conflict, Resolution, Peace, Communication, Compassionate Communication, Giraffe, Harmony at work, Peaceful Communication, Reconciliation, Christine King" />
        <script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/jquery.url.min.js" type="text/javascript"></script>
        <script src="js/siteTemplate.js" type="text/javascript"></script>
        <link href="SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function()
            {
                // NOTE: DO NOT DELETE
                Event_OnDocumentReady()
            });
             
            
        </script>
        <!-- InstanceBeginEditable name="PageStyles" -->
        <style type="text/css">
            <!--
            .SiteNotice {
                font-family: Tahoma, Geneva, sans-serif;
                font-weight: bold;
                color: #FF0;
                background-color: #00C;
                font-size: medium;
            }
            .BannerLink {
    color: #FFF;
    background-color: #F03;
    text-align: center;
    font-size: large;
            }
            .BannerLink a {
    color: #FFF;
    background-color: #FFF;
            }
            -->
        </style>
<!-- InstanceEndEditable -->
                
        <link href="css/basePage.css" rel="stylesheet" type="text/css" />
    <!-- InstanceBeginEditable name="OnDocumentReady" -->
            <script type="text/javascript">
                function Event_OnDocumentReady()
                {
                    // Place code here to .....
                }
            </script>
<!-- InstanceEndEditable -->
        <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-29773787-1']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
    </head>
    <body>
        <div id="contain"> 
            <div id="header">
                <table width="100%" border="0">
                      <tr>
                        <td colspan="3">
                            <img src="img/IndexBanner.jpg" alt="Talking Peace banner" width="984" height="145" />
                        </td>
                    </tr>
                      <tr>
                        <td width="30%">&nbsp;</td>
                        <td width="40%">&nbsp;</td>
                        <td width="30%">&nbsp;</td>
                      </tr>
                </table>
            </div>
            <div id="leftcol"> 
                <ul id="MenuBar1" class="MenuBarVertical">
                  <li><a href="index.php" >Home</a></li>
                  <li><a href="http://spiritualworkshops.co.uk/wordpress/">Members Area</a></li>
                  <li><a href="#" target="new" class="MenuBarItemSubmenu">Purchase CD's</a>
                    <ul>
                      <li><a href=" http://wordpress.spiritualworkshops.co.uk/?page_id=802">Listen Before You Buy</a></li>
                      <li><a href="http://www.spiritualworkshops.eu/products_categories/cd-audiomeditations/" title="Shipped CD's" target="new">CD's</a></li>
                      <li><a href="http://www.spiritualworkshops.eu/products_categories/digital-download/" target="new">Digital Downloads</a></li>
                    </ul>
                  </li>
                  <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=344" >Contacts / About</a></li>
                  <li><a href=" http://wordpress.spiritualworkshops.co.uk/?page_id=766" >Latest Workshops</a></li>
                  <li><a href="http://spiritualworkshops.co.uk/wordpress/?page_id=394">Spiritual solutions for everyday challenges</a></li>
<li><a href=" http://wordpress.spiritualworkshops.co.uk/?page_id=148" >Previous Workshops</a></li>
<li><a href="#" class="MenuBarItemSubmenu">Photo Galleries</a>
             <ul>
                              <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=625">Broadstairs</a></li>
                              <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=643">France</a></li>
                              <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=648">France-Scarabee</a></li>
                              <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=652">Glastonbury</a></li>
                              <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=656">Malta</a></li>
                              <li><a href="http://wordpress.spiritualworkshops.co.uk/?page_id=660">Stonehenge / Cropcircles</a></li>
                        </ul>
                      </li>
                      <li><a href="#" title="Tours" target="_blank" class="MenuBarItemSubmenu">Tours</a>
                        <ul>
                              <li><a href="http://www.spiritualtours.co.uk/?attachment_id=58">Egypt</a></li>
                              <li><a href="http://www.spiritualtours.co.uk/?attachment_id=49" target="_blank">Malta and Gozo</a></li>
                        </ul>
                      </li>
</ul>
                <br/>
                <div id="FaceBookLink">
                    <!-- Facebook Badge START -->
                    <a href="http://www.facebook.com/people/Christine-King/100000821937332" target="_TOP"
                       style="font-family: &quot;lucida grande&quot;,tahoma,verdana,arial,sans-serif; font-size: 11px; font-variant:
                              normal; font-style: normal; font-weight: normal; color: #3B5998; text-decoration: none;"
                       title="Christine King">Christine King
                    </a>
                    <br/>
                    <a href="http://www.facebook.com/people/Christine-King/100000821937332" target="_blank" title="Christine King">
                        <img src="img\ChrisKingCoral.jpg" width="149" height="273" style="border: 0px;" />
                    </a>
                    <!-- Facebook Badge END -->
                </div>
            </div>
            <div id="content"> 
                <!-- InstanceBeginEditable name="PageContent" -->
                <h4>
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="2" align="center">
                                <div class="BannerLink">
                                      <strong>NEW!</strong>: <a href="http://www.spiritualworkshops.eu/categories/" target="new" class="BannerLink">Click here to puchase CD's online</a><br/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a href=" http://wordpress.spiritualworkshops.co.uk/?page_id=766">
                                    <img src="img/Button_LatestWorkshops_Coral.jpg"
                                         alt="Christine's latest workshops"
                                         width="262"
                                         height="45"
                                         border="1" />
                                </a>
                            </td>
                            <td align="left">
                                <a href="http://spiritualworkshops.co.uk/wordpress/?page_id=148">
                                    <img src="img/Button_PreviousWorkshops_Coral.jpg"
                                         alt="Christine's archieve workshops"
                                         width="262"
                                         height="45"
                                         border="1" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="http://spiritualworkshops.co.uk/wordpress/?page_id=5">
                                    <img src="img/Button_FreeVideos_Coral.jpg" 
                                         alt="Free Videos" 
                                         width="262"
                                         height="45" 
                                         border="1" />
                                </a>
                            </td>
                            <td align="left">
                                <a href="http://spiritualworkshops.co.uk/wordpress/?page_id=93">
                                    <img src="img\Button_FreeAudio_Coral.jpg"
                                         alt="Free Audio"
                                         width="262"
                                         height="45"
                                         border="1" />
                                 </a>
                             </td>
                        </tr>
                    </table>
                </h4>
                <div>
                    <iframe width="480" height="360" src="http://www.youtube.com/embed/qFEjQC76UEw?rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <div>
                    <h4>Access to the Metaphysical Society website can be found on the links page</h4>
                    <p>Hello, I am really pleased you�ve browsed to my website. There is so much here to show you and more exciting material and events are being added all the time. If we haven�t �met� before, do just take a few minutes to watch the above video as It will give you an intro to my work. If you want to explore more, you can get access to over 17 of my audios plus several informative and interactive videos for personal and spiritual transformation, all of them are <b>ABSOLUTELY FREE</b></p>
                    <p>Click <a href="http://spiritualworkshops.co.uk/wordpress/?page_id=93">here</a> to access the audio pages and <a href=http://spiritualworkshops.co.uk/wordpress/?page_id=5">here</a> for the video pages.</p>
                    <h4>STOP PRESS</h4>
                    <p>Please bear with us. In order to bring you lots of exciting additional material and to make it easier to navigate the site, we are in process of updating all our technology.  Once complete you will be able to get access to online courses, video training programmes, webinars, podcasts and the latest meditation and tapping audio/visuals.  To register  for this great new up and coming material please just sign in with your name and email address on the members page and you will be amongst the first to get access once it is all ready.</p>
                    <p>Meanwhile you can get a taster by watching lots of my free introductory videos on this youtube link <a href="http://www.youtube.com/user/Infinitekristy/videos" target="_blank">http://www.youtube.com/user/Infinitekristy/videos</a>  and all these free audios on this link</p>
                    <p><b>Check out the latest workshops click</b> <a href="CurrentWorkshops.htm">here.</a></p>
                </div>
<!-- InstanceEndEditable -->
                <div align="center">
                    <br />
                    Site layout and design by <a href="mailto:helen@imagesteps.co.uk">ImageSteps Limited</a> 2012<br />
                </div>
            </div>
        </div>
        <script type="text/javascript">
            <!--
            var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
            //-->
        </script>
    </body>
<!-- InstanceEnd --></html>
