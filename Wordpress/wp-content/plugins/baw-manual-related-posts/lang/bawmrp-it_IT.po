msgid ""
msgstr ""
"Project-Id-Version: BAW Manual Related Posts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-11-01 13:27+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: Manolo <m_macchetta@yahoo.com>\n"
"Language-Team: Juliobox <juliobosk@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e;_n;esc_attr_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-Language: French\n"
"X-Poedit-Country: FRANCE\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-SearchPath-0: c:\\Users\\Julio\\Dropbox\\BAW\\plugins\\baw-manual-related-posts_repo\\trunk\n"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:42
msgid "Title"
msgstr "Titolo"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:42
msgid "Type"
msgstr "Tipo"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:42
msgid "Date"
msgstr "Data"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:42
msgid "Status"
msgstr "Status"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:48
msgid "Published"
msgstr "Pubblicato"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:51
msgid "Scheduled"
msgstr "Pianificato"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:54
msgid "Pending Review"
msgstr "Manca la correzione"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:57
msgid "Draft"
msgstr "Bozza"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-ajax.inc.php:64
msgid "Y/m/d"
msgstr "A/m/g"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:10
msgid "Find related posts"
msgstr "Trova Articoli Correlati"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:16
#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:18
msgid "Search"
msgstr "Cerca"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:34
msgid "Close"
msgstr "Chiudi"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:35
msgid "Select"
msgstr "Seleziona"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:59
msgid "Add a related post"
msgstr "Aggiungi un articolo correlato"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:60
msgid "Clear all"
msgstr "Pulisci tutti"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:61
msgid "Add posts IDs from posts you want to relate, comma separated."
msgstr "Aggiungi l ID del post che vuoi correlare, separato da una virgola"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:110
msgid "Settings"
msgstr "Configurazione"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:138
msgid "General"
msgstr "Generale"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:139
msgid "How many posts maximum"
msgstr "Quanti articoli al massimo"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:140
msgid "Randomize related posts"
msgstr "Articoli Casuali"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:141
msgid "Select post types"
msgstr "Seleziona il tipo di articolo"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:142
msgid "Caching data"
msgstr "Caching data"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:143
msgid "Display"
msgstr "Mostra"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:144
msgid "Display related posts in post content"
msgstr "Mostra articoli correlati nel contenuto articoli"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:145
msgid "Display related posts in home page too"
msgstr "Mostra articoli correlati anche nella homepage"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:146
msgid "Display mode"
msgstr "Modalità di Visione"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:147
msgid "Display content?"
msgstr "Mostra contenuti ?"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:148
msgid "Display something is no related posts are found?"
msgstr "Mostra se non sono stati trovati contenuti correlati ?"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:149
msgid "Auto posts"
msgstr "Articoli automatici"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:150
msgid "Use auto related posts to fill the max posts ?"
msgstr "Usa articoli automatici per riempire il numero massimo di spazi ?"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:151
msgid "Use sticky posts to fill the max posts ?"
msgstr "Usa articoli Sticky per riempire il numero massimo di spazi ?"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:152
msgid "Use recent posts to fill the max posts ?"
msgstr "Usa articoli recenti per riempire il numero massimo di spazi ?"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/backend-noajax.inc.php:153
msgid "About"
msgstr "Circa"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/bothend-noajax.inc.php:15
msgid "No related posts found!"
msgstr "Non sono stati trovati articoli correlati !"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/frontend-noajax.inc.php:29
#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:23
msgid "You may also like:"
msgstr "Potrebbe piacerti:"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:9
msgid "Will be displayed at bottom of content."
msgstr "Mostrato alla fine del contenuto:"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:15
msgid "So ironic ... auto related posts in manual related posts ;)"
msgstr "Ironia della sorte... Post automatici un un plug-in chiamato Post Manuali ;)"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:25
#, php-format
msgid "Front-end title for %s:"
msgstr "Titolo per %s :"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:33
msgid "Do not show anything."
msgstr "Non mostrare nulla"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:35
msgid "HTML allowed."
msgstr "HTML autorizzati."

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:43
msgid "No thank you, i only need my manual posts."
msgstr "No grazie Ho bisogno soltanto di post manuali"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:45
msgid "Use tags and categories to find related posts."
msgstr "Usa Tags e categorie per trovare i post correlati"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:47
msgid "Use only tags."
msgstr "Usa solo i tags"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:49
msgid "Use ony categories."
msgstr "Usa solo le categorie"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:57
msgid "How many days do we have to cache results ? (min. 1)"
msgstr "Per quanti giorni dobbiamo tenere in cache i risultati (min =1 )"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:65
msgid "Including manual and auto related posts. (0 = No limit)"
msgstr "Includi Articoli correlati manuali e automatici ( con 0 = senza limiti)"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:73
msgid "You can randomize the display for posts order."
msgstr "Puoi disporre a caso gli articoli più vecchi"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:74
msgid "You can randomize the posts request."
msgstr "Puoi mettere gli articoli a caso."

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:82
msgid "Sticky posts will be included if needed."
msgstr ""

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:90
msgid "Recents posts will be included if needed."
msgstr "Gli articoli più recenti saranno inclusi se necessario"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:99
msgid "No content. You can add you own using the <code>bawmrp_more_content</code> filter hook."
msgstr "Nessun contenuto. Puoi aggiungere il tuo usando il filtro <code>bawmrp_more_content</code>"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:101
msgid "Print the post excerpt. <span style=\"font-size: smaller\">(This adds 1 more query per related posts. I suggest you to active HTML cache, see below.)</span>"
msgstr "Mostra il riassunto <span style=\"font-size: smaller\">(Questo aggiunge una richiesta in più ai post correlati. Ti suggerisco di attivare la cache HTML, vedi sotto.)</span>"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:103
msgid "Print the post content <span style=\"font-size: smaller\">(This adds 1 more query per related posts. I suggest you to active HTML cache, see below.)</span>"
msgstr "Stampa il contenuto dell'articolo. <span style=\"font-size: smaller\">(Aggiunge una richiesta in più nei post correlati. Ti suggeriamo di attivare la Cache HTML.)</span>"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:112
msgid "List mode."
msgstr "Modalità Lista"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:114
msgid "Thumb mode."
msgstr "Modalità con Icone"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:116
msgid "Shortcode mode. Use <code>&lt;?php echo do_shortcode( '[manual_related_posts]' ); ?&gt</code> to pull the list out."
msgstr "Shortcode. Usa <code>&lt;?php echo do_shortcode( '[manual_related_posts]' ); ?&gt</code> per avere la lista"

#: c:\Users\Julio\Dropbox\BAW\plugins\baw-manual-related-posts_repo\trunk/inc/setting_fields.inc.php:124
msgid "Will be displayed at bottom of content on home page."
msgstr "Mostrato ai piedi del contenuto della Home"

#~ msgid ""
#~ "You can choose \"4\" and add more than 4 in the meta box, then use "
#~ "\"Random posts\" to view different related posts each time. \"0\" = No "
#~ "limit"
#~ msgstr ""
#~ "Vous pouvez choisir \"4\" et ajouter plus de 4 dans la meta box, ensuite "
#~ "utiliser \"Articles en relation aléatoires\" pour visionner des articles "
#~ "différents à chaque fois. \"0\" = Pas de limite"

#~ msgid "Head title for front-end (if previous checkbox is checked)"
#~ msgstr ""
#~ "Title de l'entête pour le front-end (si la case précédente est cochée)"
