<?php

class E_UploadException extends RuntimeException
{
	function __construct($message='', $code=NULL, $previous=NULL)
	{
		if (!$message) $message = "There was a problem uploading the file.";
		parent::__construct($message, $code, $previous);
	}
}

class E_InsufficientWriteAccessException extends RuntimeException
{
	function __construct($message=FALSE, $filename=NULL, $code=NULL, $previous=NULL)
	{
		if (!$message) $message = "Could not write to file. Please check filesystem permissions.";
		if ($filename) $message .= " Filename: {$filename}";
		parent::__construct($message, $code, $previous);
	}
}

class E_NoSpaceAvailableException extends RuntimeException
{
	function __construct($message='', $code=NULL, $previous=NULL)
	{
		if (!$message) $message = "You have exceeded your storage capacity. Please remove some files and try again.";
		parent::__construct($message, $code, $previous);
	}
}

class Mixin_GalleryStorage_Driver_Base extends Mixin
{
	/**
	 * Set correct file permissions (taken from wp core). Should be called
	 * after writing any file
	 *
	 * @class nggAdmin
	 * @param string $filename
	 * @return bool $result
	 */
	function _chmod($filename = '')
	{
		$stat = @ stat( dirname($filename) );
		$perms = $stat['mode'] & 0000666; // Remove execute bits for files
		if ( @chmod($filename, $perms) )
			return TRUE;

		return FALSE;
	}

    /**
     * Gets the id of a gallery, regardless of whether an integer
     * or object was p	if ($crop_ratio_x < $crop_ratio_y)
							{
								$crop_width = max($crop_width, $width);
								$crop_height = (int) round($crop_width / $aspect_ratio);
							}
							else
							{
								$crop_height = max($crop_height, $height);
								$crop_width = (int) round($crop_height * $aspect_ratio);
							}

							if ($crop_width == ($crop_width_orig - 1))
							{
								$crop_width = $crop_width_orig;
							}

							if ($crop_height == ($crop_height_orig - 1))
							{
								$crop_height = $crop_height_orig;
							}
						}

						$crop_diff_x = (int) round(($crop_width_orig - $crop_width) / 2);
						$crop_diff_y = (int) round(($crop_height_orig - $crop_height) / 2);

						$crop_x += $crop_diff_x;
						$crop_y += $crop_diff_y;

						$crop_max_x = ($crop_x + $crop_width);
						$crop_max_y = ($crop_y + $crop_height);

						// Check if we're overflowing borders
						//
						if ($crop_x < 0)
						{
							$crop_x = 0;
						}
						else if ($crop_max_x > $original_width)
						{
							$crop_x -= ($crop_max_x - $original_width);
						}

						if ($crop_y < 0)
						{
							$crop_y = 0;
						}
						else if ($crop_max_y > $original_height)
						{
							$crop_y -= ($crop_max_y - $original_height);
						}
					}
					else
					{
						if ($orig_ratio_x < $orig_ratio_y)
						{
							$crop_width = $original_width;
							$crop_height = (int) round($height * $orig_ratio_x);

						}
						else
						{
							$crop_height = $original_height;
							$crop_width = (int) round($width * $orig_ratio_y);
						}

						if ($crop_width == ($width - 1))
						{
							$crop_width = $width;
						}

						if ($crop_height == ($height - 1))
						{
							$crop_height = $height;
						}

						$crop_x = (int) round(($original_width - $crop_width) / 2);
						$crop_y = (int) round(($original_height - $crop_height) / 2);
					}

					$result['crop_area'] = array('x' => $crop_x, 'y' => $crop_y, 'width' => $crop_width, 'height' => $crop_height);
				}
				else {
					// Just constraint dimensions to ensure there's no stretching or deformations
					list($width, $height) = wp_constrain_dimensions($original_width, $original_height, $width, $height);
				}
			}

			$result['width'] = $width;
			$result['height'] = $height;
			$result['quality'] = $quality;

			$real_width = $width;
			$real_height = $height;

			if ($rotation && in_array(abs($rotation), array(90, 270)))
			{
				$real_width = $height;
				$real_height = $width;
			}

			if ($reflection)
			{
				// default for nextgen was 40%, this is used in generate_image_clone as well
				$reflection_amount = 40;
				// Note, round() would probably be best here but using the same code that C_NggLegacy_Thumbnail uses for compatibility
        $reflection_height = intval($real_height * ($reflection_amount / 100));
        $real_height = $real_height + $reflection_height;
			}

			$result['real_width'] = $real_width;
			$result['real_height'] = $real_height;
		}

		return $result;
	}

	/**
	 * Returns an array of dimensional properties (width, height, real_width, real_height) of a resulting clone image if and when generated
	 * @param string $image_path
	 * @param string $clone_path
	 * @param array $params
	 * @return array
	 */
	function calculate_image_clone_dimensions($image_path, $clone_path, $params)
	{
		$retval = null;
		$result = $this->object->calculate_image_clone_result($image_path, $clone_path, $params);

		if ($result != null) {
			$retval = array(
				'width' => $result['width'],
				'height' => $result['height'],
				'real_width' => $result['real_width'],
				'real_height' => $result['real_height']
			);
		}

		return $retval;
	}

	/**
	 * Generates a "clone" for an existing image, the clone can be altered using the $params array
	 * @param string $image_path
	 * @param string $clone_path
	 * @param array $params
	 * @return object
	 */
	function generate_image_clone($image_path, $clone_path, $params)
	{
		$width      = isset($params['width'])      ? $params['width']      : NULL;
		$height     = isset($params['height'])     ? $params['height']     : NULL;
		$quality    = isset($params['quality'])    ? $params['quality']    : NULL;
		$type       = isset($params['type'])       ? $params['type']       : NULL;
		$crop       = isset($params['crop'])       ? $params['crop']       : NULL;
		$watermark  = isset($params['watermark'])  ? $params['watermark']  : NULL;
		$reflection = isset($params['reflection']) ? $params['reflection'] : NULL;
		$rotation   = isset($params['rotation']) ? $params['rotation'] : NULL;
		$flip   = isset($params['flip']) ? $params['flip'] : NULL;
		$crop_frame = isset($params['crop_frame']) ? $params['crop_frame'] : NULL;
		$destpath   = NULL;
		$thumbnail  = NULL;

		$result = $this->object->calculate_image_clone_result($image_path, $clone_path, $params);

		// XXX this should maybe be removed and extra settings go into $params?
		$settings = C_NextGen_Settings::get_instance();

		// Ensure we have a valid image
		if ($image_path && file_exists($image_path) && $result != null && !isset($result['error']))
		{
			$image_dir = dirname($image_path);
			$clone_path = $result['clone_path'];
			$clone_dir = $result['clone_directory'];
			$clone_suffix = $result['clone_suffix'];
			$clone_format = $result['clone_format'];
			$format_list = $this->object->get_image_format_list();

			// Ensure target directory exists, but only create 1 subdirectory
			if (!file_exists($clone_dir))
			{
				if (strtolower(realpath($image_dir)) != strtolower(realpath($clone_dir)))
				{
					if (strtolower(realpath($image_dir)) == strtolower(realpath(dirname($clone_dir))))
					{
						wp_mkdir_p($clone_dir);
					}
				}
			}

			$method = $result['method'];
			$width = $result['width'];
			$height = $result['height'];
			$quality = $result['quality'];
			
			if ($quality == null)
			{
				$quality = 100;
			}

			if ($method == 'wordpress')
			{
                $original = wp_get_image_editor($image_path);
                $destpath = $clone_path;
                if (!is_wp_error($original))
                {
                    $original->resize($width, $height, $crop);
                    $original->set_quality($quality);
                    $original->save($clone_path);
                }
			}
			else if ($method == 'nextgen')
			{
				$destpath = $clone_path;
				$thumbnail = new C_NggLegacy_Thumbnail($image_path, true);

				if ($crop) {
					$crop_area = $result['crop_area'];
					$crop_x = $crop_area['x'];
					$crop_y = $crop_area['y'];
					$crop_width = $crop_area['width'];
					$crop_height = $crop_area['height'];

					$thumbnail->crop($crop_x, $crop_y, $crop_width, $crop_height);
				}

				$thumbnail->resize($width, $height);
			}

			// We successfully generated the thumbnail
			if (is_string($destpath) && (file_exists($destpath) || $thumbnail != null))
			{
				if ($clone_format != null)
				{
					if (isset($format_list[$clone_format]))
					{
						$clone_format_extension = $format_list[$clone_format];
						$clone_format_extension_str = null;

						if ($clone_format_extension != null)
						{
							$clone_format_extension_str = '.' . $clone_format_extension;
						}

						$destpath_info = pathinfo($destpath);
						$destpath_extension = $destpath_info['extension'];
						$destpath_extension_str = null;

						if ($destpath_extension != null)
						{
							$destpath_extension_str = '.' . $destpath_extension;
						}

						if (strtolower($destpath_extension) != strtolower($clone_format_extension))
						{
							$destpath_dir = $destpath_info['dirname'];
							$destpath_basename = $destpath_info['filename'];
							$destpath_new = $destpath_dir . DIRECTORY_SEPARATOR . $destpath_basename . $clone_format_extension_str;

							if ((file_exists($destpath) && rename($destpath, $destpath_new)) || $thumbnail != null)
							{
								$destpath = $destpath_new;
							}
						}
					}
				}

				if (is_null($thumbnail))
				{
					$thumbnail = new C_NggLegacy_Thumbnail($destpath, true);
				}
				else
				{
					$thumbnail->fileName = $destpath;
				}

				// This is quite odd, when watermark equals int(0) it seems all statements below ($watermark == 'image') and ($watermark == 'text') both evaluate as true
				// so we set it at null if it evaluates to any null-like value
				if ($watermark == null)
				{
					$watermark = null;
				}
				
				if ($watermark == 1 || $watermark === true)
				{
					if (in_array(strval($settings->wmType), array('image', 'text')))
					{
						$watermark = $settings->wmType;
					}
					else
					{
						$watermark = 'text';
					}
				}
				
				$watermark = strval($watermark);

				if ($watermark == 'image')
				{
					$thumbnail->watermarkImgPath = $settings['wmPath'];
					$thumbnail->watermarkImage($settings['wmPos'], $settings['wmXpos'], $settings['wmYpos']);
				}
				else if ($watermark == 'text')
				{
					$thumbnail->watermarkText = $settings['wmText'];
					$thumbnail->watermarkCreateText($settings['wmColor'], $settings['wmFont'], $settings['wmSize'], $settings['wmOpaque']);
					$thumbnail->watermarkImage($settings['wmPos'], $settings['wmXpos'], $settings['wmYpos']);
				}

				if ($rotation && in_array(abs($rotation), array(90, 180, 270)))
				{
					$thumbnail->rotateImageAngle($rotation);
				}

				$flip = strtolower($flip);

				if ($flip && in_array($flip, array('h', 'v', 'hv')))
				{
					$flip_h = in_array($flip, array('h', 'hv'));
					$flip_v = in_array($flip, array('v', 'hv'));

					$thumbnail->flipImage($flip_h, $flip_v);
				}

				if ($reflection)
				{
					$thumbnail->createReflection(40, 40, 50, FALSE, '#a4a4a4');
				}

				if ($clone_format != null && isset($format_list[$clone_format]))
				{
					// Force format
					$thumbnail->format = strtoupper($format_list[$clone_format]);
				}

				$thumbnail->save($destpath, $quality);
			}
		}

		return $thumbnail;
	}
}

class C_GalleryStorage_Driver_Base extends C_GalleryStorage_Base
{
    public static $_instances = array();

	function define($context)
	{
		parent::define($context);
		$this->add_mixin('Mixin_GalleryStorage_Driver_Base');
		$this->implement('I_GalleryStorage_Driver');
	}

	function initialize()
	{
		parent::initialize();
		$this->_gallery_mapper = $this->get_registry()->get_utility('I_Gallery_Mapper');
		$this->_image_mapper = $this->get_registry()->get_utility('I_Image_Mapper');
	}

    public static function get_instance($context = False)
    {
        if (!isset(self::$_instances[$context]))
        {
            self::$_instances[$context] = new C_GalleryStorage_Driver_Base($context);
        }
        return self::$_instances[$context];
    }


	/**
	 * Gets the class name of the driver used
	 * @return string
	 */
	function get_driver_class_name()
	{
		return get_called_class();
	}
}
