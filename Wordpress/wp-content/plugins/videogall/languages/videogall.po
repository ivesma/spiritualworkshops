msgid ""
msgstr ""
"Project-Id-Version: VideoGall\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-02-16 15:35-0600\n"
"PO-Revision-Date: 2013-02-16 15:35-0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e;_x;_n;gettext;gettext_noop\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: C:\\Users\\a509597\\Applications\\EasyPHP-5.3.8.1\\www\\wptest\\wordpress\\wp-content\\plugins\\videogall\n"

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/videogall.php:107
msgid "Category: "
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:51
msgid "Small"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:52
msgid "Medium"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:53
msgid "Large"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:60
msgid "1"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:61
msgid "2"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:62
msgid "3"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:63
msgid "4"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:64
msgid "5"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:65
msgid "6"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:72
msgid "New to Old"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:73
msgid "Old to New"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:83
msgid "My Videos"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:84
#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:233
msgid "Add new video"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:85
msgid "Add/Update categories"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:86
msgid "Additional Settings"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:116
msgid "Videogall Settings"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:128
msgid "Default settings restored"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:138
#, php-format
msgid "%s Settings"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:159
msgid "Edit video"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:165
msgid "Video URL *"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:169
msgid "Video thumbnail"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:171
#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:211
msgid "Upload thumbnail"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:174
msgid "Video category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:176
#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:216
msgid "Uncategorized"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:187
msgid "Video caption"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:191
msgid "Video description"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:194
msgid "Save Video Changes"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:201
msgid "Delete Videos"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:205
msgid "Add video url *"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:209
msgid "Add video thumbnail"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:214
msgid "Select video category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:226
msgid "Add video caption"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:230
msgid "Add video description"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:237
msgid "Add category name *"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:239
msgid "Add new category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:242
msgid "Update or Delete category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:244
#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/inc/videogall-widget.php:61
msgid "Select category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:252
msgid "Delete category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:255
msgid "Rename Category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:257
msgid "Update category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:262
msgid "Number of columns"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:270
msgid "Video Size"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:278
msgid "Enable Pagination ?"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:282
msgid "Videos Per Page"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:286
msgid "Show categories"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:290
msgid "Sort videos by category"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:294
msgid "Video Order"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:302
msgid "Show border around thumbnail"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:306
msgid "Border Color"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:310
msgid "Show Facebook Like Button"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:314
msgid "Use shadowbox for images"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:323
msgid "Reset Additional Settings"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:328
msgid "PayPal - The safer, easier way to pay online!"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:328
msgid "Donate via PayPal"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:350
msgid "Videos per page should be a number"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:375
msgid "Videos cannot be deleted"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:376
msgid "Videos deleted"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:378
#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:380
msgid "No videos selected. Select/Unselect videos to delete by clicking on them"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:396
msgid "Video not updated"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:397
msgid "Video Updated"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:399
msgid "Name & URL are required"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:411
msgid "Category cannot be updated"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:412
msgid "Category Updated"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:415
msgid "Select category from dropdown to update"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:422
msgid "Category cannot be deleted"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:426
msgid "Category deleted"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:436
msgid "Category cannot be added"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:437
msgid "New Category Added"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:439
msgid "Category name is required"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:454
msgid "Video cannot be added"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:455
msgid "New Video Added"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/admin/plugin-options.php:457
msgid "URL is required"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/inc/videogall-widget.php:5
msgid "Display video gallery on sidebar"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/inc/videogall-widget.php:6
msgid "Videogall"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/inc/videogall-widget.php:55
msgid "Title (optional):"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/inc/videogall-widget.php:59
msgid "Category:"
msgstr ""

#: C:\Users\a509597\Applications\EasyPHP-5.3.8.1\www\wptest\wordpress\wp-content\plugins\videogall/inc/videogall-widget.php:71
msgid "Number of videos:"
msgstr ""

